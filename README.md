# Django project deployed in AWS ECS infrastructure and automated using Gitlab CI

The goal of this project is to make an end-to-end django project involving CI/CD pipeline and deploy it in AWS ECS services using terraform.

## Overall infrastructure diagram

![Infrastructure](/diagrams/infrastructure.jpeg)

## Architecture breakdown at EC2 instance level

![Infrastructure](/diagrams/Instance_level_architecture.jpeg)

### AWS services used
1. Application load balancer (Distribute requests to different EC2 instances)
2. Auto-scaling groups (to increase or decrease EC2 instances based on traffic)
3. RDS Postgres DB instance
4. EC2 instance acting as bastion host/ jumpbox placed in public subnet to ssh into other EC2 instances placed in private subnet
5. VPC 
   5.1 (2 public subnets and 2 private subnets)
   5.2 RDS instance is placed in private subnet
   5.3 EC2 instances running django application placed in private subnets

### Note:
1. When the docker container boots up, using app/migrations.sh, it first checks if the tables in the RDS postgres have already been created to avoid migration conflicts.
2. If tables already exists, it applies fake migration, else it will migrate tables to DB