variable "bastion-instance-type" {
  default = "t2.micro"
}

variable "webserver-instance-type" {
  default = "t2.small"
}

variable "container-port" {
  default = 8000
}

variable "host-port" {
  default = 8000
}

variable "environment" {
  default = "stable"
}

variable "cluster_name" {
  default = "end-to-end-ecs-project-cluster"
}

# for docker image and tag
variable "img_tag" {}
variable "ecr_repo" {}