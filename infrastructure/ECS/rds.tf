# RDS
resource "aws_db_instance" "postgresql" {
  engine               = "postgres"
  engine_version       = "15.3"
  multi_az             = false
  identifier           = "postgres1"
  instance_class       = "db.t3.micro"
  allocated_storage    = 5
  db_subnet_group_name = aws_db_subnet_group.database-subnet-group.name
  storage_encrypted    = false
  availability_zone    = "us-east-1a"
  db_name              = "postgres1"   # need to use this value in django db config
  port                 = "5432"
  skip_final_snapshot  = true
  publicly_accessible = true

  username = "rahib"
  password = "rahib123"

  vpc_security_group_ids = [aws_security_group.rds-sg.id]
}

# RDS sg
resource "aws_security_group" "rds-sg" {
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "rds-sg"
  }
}

resource "aws_db_subnet_group" "database-subnet-group" {
  name       = "dbsubnetgroup"
  subnet_ids = [aws_subnet.public_1.id, aws_subnet.public_2.id]

  tags = {
    Name = "db-subnet-group"
  }
}