#!/bin/bash

# Run the check_tables_exist management command
if python manage.py check_table_exists; then
    # Tables exist, run migrations with --fake
    echo "Table exists. Applying fake migrations!!!"
    python manage.py migrate --fake

else
    # Tables do not exist, run migrations as usual
    echo "Tables do not exist. Applying migrations!!!"
    python manage.py makemigrations
    python manage.py sqlmigrate employee_register 0001
    python manage.py migrate
fi