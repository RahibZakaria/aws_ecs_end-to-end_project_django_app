import psycopg2
import os
import logging

logger = logging.getLogger("check_tables")


#database_name, user, password, host, port
def check_tables_exist():
    try:
        # Connect to the PostgreSQL database
        logger.info("Connecting to RDS DB")
        conn = psycopg2.connect(
            database='postgres1',
            user='rahib',
            password='rahib123',
            host="postgres1.cjvxwyy0irl2.us-east-1.rds.amazonaws.com",#os.environ.get('RDS_HOSTNAME'),
            port='5432'
        )

        # Create a cursor object to interact with the database
        cursor = conn.cursor()

        # Query to retrieve a list of table names in the public schema
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';")

        # Fetch all table names
        table_names = [row[0] for row in cursor.fetchall()]

        # Close the cursor and connection
        cursor.close()
        conn.close()

        return table_names
    except Exception as e:
        print(f"Error: {e}")
        logger.error(str(e))
        return []


if __name__ == "__main__":

    # Check if tables exist
    table_names = check_tables_exist()

    if table_names:
        print("Tables exist in the database:")
        for table_name in table_names:
            print(table_name)
    else:
        print("No tables found in the database.")
