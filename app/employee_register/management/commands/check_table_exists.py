from django.core.management.base import BaseCommand
from ...utils.check_tables_exist import check_tables_exist  # Import the table check script

class Command(BaseCommand):
    help = 'Check if tables exist in the database before running migrations'

    def handle(self, *args, **options):
        # Call the table check function
        table_names = check_tables_exist()

        if table_names:
            self.stdout.write(self.style.SUCCESS(f"Tables exist in the database: {', '.join(table_names)}"))
        else:
            self.stdout.write(self.style.SUCCESS("No tables found in the database."))
